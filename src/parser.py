import argparse


def parse(registries: list[str, ...]):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--registry',
        choices=registries,
        required=True,
    )
    return parser.parse_args()
